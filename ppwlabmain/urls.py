from django.urls import include, path

from . import views

app_name = "ppwlabmain"

urlpatterns = [
    path('', views.index, name="index"),
]
