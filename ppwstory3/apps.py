from django.apps import AppConfig

postdict = {}
desdict = {}

class Ppwstory3Config(AppConfig):
    name = 'ppwstory3'

    def ready(self):
        from pathlib import Path
        import os

        BASE_DIR = Path(__file__).resolve().parent.parent

        postdict.clear()
        desdict.clear()
        for basepath, dirname, filenames in os.walk(str(BASE_DIR / "templates" / "ppwstory3" / "posts")):
            for filename in filenames:
                file = open(os.path.join(basepath, filename))
                content = file.read().strip()
                title = content[content.index("{% block title %}")+17:content.index("{% endblock %}")]
                postdict[int(filename.split(".")[0])]  = title
                text = content.split("<p>")[1]
                description = text.split(".")[0] + "."
                desdict[int(filename.split(".")[0])] = description
