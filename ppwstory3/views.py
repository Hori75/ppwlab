from django.http import Http404
from django.shortcuts import redirect, render, reverse
from django.template import TemplateDoesNotExist

from .apps import postdict, desdict

def index(request):
    return render(request, 'ppwstory3/index.html', {'postdict':postdict, "desdict": desdict, "range":range(1,len(postdict)+1)})

def profile(request):
    return render(request, 'ppwstory3/profile.html')

def posts(request, number=0):
    if (number==0):
        return redirect(reverse('ppwstory3:index'))
    else:
        return render(request, 'ppwstory3/posts/' + str(number) + ".html")
