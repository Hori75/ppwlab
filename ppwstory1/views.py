from django.shortcuts import render

def index(request):
    return render(request, 'ppwstory1/index.html')

def viewProfile(request):
    return render(request, 'ppwstory1/profile.html')
